package ru.nsu.ccfit.ermakova.Parser;

public enum MethodsEnum {
    POST_METHOD,
    GET_METHOD;

    public static final String POST_METHOD_STR = "POST";
    public static final String GET_METHOD_STR = "GET";

    public static MethodsEnum fromString(String str) {
        if (str.equals(POST_METHOD_STR)) return POST_METHOD;
        if (str.equals(GET_METHOD_STR)) return GET_METHOD;
        return null;
    }

}
