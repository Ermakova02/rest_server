package ru.nsu.ccfit.ermakova.Parser;

public enum URIsEnum {
    LOGIN_URI,
    LOGOUT_URI,
    USERS_URI,
    MESSAGE_URI,
    MESSAGES_LIST_URI;

    public static final String LOGIN_URI_STR = "/login";
    public static final String LOGOUT_URI_STR = "/logout";
    public static final String USERS_URI_STR = "/users";
    public static final String MESSAGE_URI_STR = "/messages";
    public static final String MESSAGES_LIST_STR = "/messages?";

    public static URIsEnum fromString(String str) {
        if (str.equals(LOGIN_URI_STR)) return LOGIN_URI;
        if (str.equals(LOGOUT_URI_STR)) return LOGOUT_URI;
        if (str.equals(USERS_URI_STR)) return USERS_URI;
        if (str.equals(MESSAGE_URI_STR)) return MESSAGE_URI;
        if (str.indexOf(MESSAGES_LIST_STR) == 0) return MESSAGES_LIST_URI;
        return null;
    }
}
