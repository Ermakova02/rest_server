package ru.nsu.ccfit.ermakova.ChatServer;

import ru.nsu.ccfit.ermakova.Parser.*;

import java.io.*;
import java.net.Socket;
import java.util.UUID;

public class SocketProcessor implements Runnable {
    private Thread t;
    private ServerProcessor srvProcessor;
    private Socket clientSocket;
    private InputStream clientInput;
    private OutputStream clientOutput;

    SocketProcessor(Socket s, ServerProcessor srv) throws IOException {
        srvProcessor = srv;
        clientSocket = s;
        clientInput = s.getInputStream();
        clientOutput = s.getOutputStream();
        t = new Thread(this, "...");
        t.start();
    }

    private void answerBadRequest() throws IOException {
        String response = "HTTP/1.1 400 Bad Request\r\n";
        clientOutput.write(response.getBytes());
        clientOutput.flush();
    }

    private void answerUserExists() throws IOException {
        String response = "HTTP/1.1 401 Unauthorized\r\n" +
                "WWW-Authenticate: Token realm='Username is already in use'";
        clientOutput.write(response.getBytes());
        clientOutput.flush();
    }

    private void answerNewUser(UserData ud) throws IOException {
        String response = "HTTP/1.1 200 OK\r\n" +
                "Content-Type: application/json\r\n" +
                "\r\n" +
                "{\r\n" +
                "    \"id\": " + Integer.toString(ud.id) + ",\r\n" +
                "    \"username\": \"" + ud.username + "\",\r\n" +
                "    \"online\": true,\r\n" +
                "    \"token\": \"" + ud.token.toString() + "\"\r\n" +
                "}";
        clientOutput.write(response.getBytes());
        clientOutput.flush();
    }

    private void answerMessageReceived(UserData ud, String msg) throws IOException {
        String response = "HTTP/1.1 200 OK\r\n" +
                "Content-Type: application/json\r\n" +
                "\r\n" +
                "{\r\n" +
                "    \"id\": " + Integer.toString(ud.id) + ",\r\n" +
                "    \"message\": \"" + msg + "\"\r\n" +
                "}";
        clientOutput.write(response.getBytes());
        clientOutput.flush();
    }

    private void answerUsersRequested() throws IOException {
        String response = "HTTP/1.1 200 OK\r\n" +
                "Content-Type: application/json\r\n" +
                "\r\n" +
                "{\r\n" +
                "    \"users\": [\r\n" +
                srvProcessor.formatUsersToJSON() +
                "    ]\r\n" +
                "}";
        clientOutput.write(response.getBytes());
        clientOutput.flush();
    }

    private void answerMessagesList(int offset, int count) throws IOException {
        String response = "HTTP/1.1 200 OK\r\n" +
                "Content-Type: application/json\r\n" +
                "\r\n" +
                "{\r\n" +
                "    \"messages\": [\r\n" +
                srvProcessor.formatMessagesToJSON(offset, count) +
                "    ]\r\n" +
                "}";
        clientOutput.write(response.getBytes());
        clientOutput.flush();
    }

    private void answerUserLoggedOut() throws IOException {
        String response = "HTTP/1.1 200 OK\r\n" +
                "Content-Type: application/json\r\n" +
                "\r\n" +
                "{\r\n" +
                "    \"message\": \"bye!\"" +
                "}";
        clientOutput.write(response.getBytes());
        clientOutput.flush();
    }

    private void answerBadToken() throws IOException {
        String response = "HTTP/1.1 403 Forbidden\r\n" +
                "WWW-Authenticate: Token realm='Bad token'";
        clientOutput.write(response.getBytes());
        clientOutput.flush();
    }

    private void answerWrongMethod() throws IOException {
        String response = "HTTP/1.1 405 Method Not Allowed\r\n" +
                "Allow: POST, GET";
        clientOutput.write(response.getBytes());
        clientOutput.flush();
    }

    private void processLogin(RestParser parser) throws BadRequestException, IOException {
        parser.moveToJSONData();
        String user = parser.getParameterValue("username");
        UserData ud = srvProcessor.userLogIn(user);
        if (ud == null) {
            answerUserExists();
        } else {
            answerNewUser(ud);
        }
    }

    private void processLogout(RestParser parser) throws IOException, BadRequestException {
        String token = parser.getTokenData();
        if (srvProcessor.userLogOut(UUID.fromString(token))) {
            answerUserLoggedOut();
        } else {
            answerBadToken();
        }
    }

    private void processMessage(RestParser parser) throws IOException, BadRequestException {
        String token = parser.getTokenData();
        UserData ud = srvProcessor.findUser(UUID.fromString(token));
        if (ud == null) {
            answerBadToken();
            return;
        }
        parser.moveToJSONData();
        String msg = parser.getParameterValue("message");
        srvProcessor.addNewMessage(false, ud.id, msg);
        answerMessageReceived(ud, msg);
    }

    private void processUsers(RestParser parser) throws IOException, BadRequestException {
        parser.nextLine();
        String token = parser.getTokenData();
        UserData ud = srvProcessor.findUser(UUID.fromString(token));
        if (ud == null) {
            answerBadToken();
            return;
        }
        srvProcessor.updateClientPresense(ud.id);
        answerUsersRequested();
    }

    private void processMessagesList(RestParser parser) throws IOException, BadRequestException {
        int offset = parser.getMessagesOffset();
        int count = parser.getMessagesCount();
        String token = parser.getTokenData();
        UserData ud = srvProcessor.findUser(UUID.fromString(token));
        if (ud == null) {
            answerBadToken();
            return;
        }
        srvProcessor.updateClientPresense(ud.id);
        answerMessagesList(offset, count);
    }

    private void processRequest() throws IOException, BadRequestException {
        StringBuilder request = new StringBuilder();
        int available = clientInput.available();
        if (available == 0) {
            try {
                Thread.sleep(ServerProcessor.BUFFER_WAIT_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        while ((available = clientInput.available()) != 0) {
            byte [] requestArray = new byte[available];
            int requestLen = clientInput.read(requestArray);
            request.append(new String(requestArray, 0, requestLen));
        }
        RestParser parser = new RestParser(request.toString());
        MethodsEnum method = parser.parseMethod();
        URIsEnum uri = null;
        switch (method) {
            case POST_METHOD:
                uri = parser.parseURI();
                switch (uri) {
                    case LOGIN_URI:
                        processLogin(parser);
                        break;
                    case LOGOUT_URI:
                        processLogout(parser);
                        break;
                    case MESSAGE_URI:
                        processMessage(parser);
                        break;
                    default:
                        // !!!
                        break;
                }
                break;
            case GET_METHOD:
                uri = parser.parseURI();
                switch (uri) {
                    case USERS_URI:
                        processUsers(parser);
                        break;
                    case MESSAGES_LIST_URI:
                        processMessagesList(parser);
                        break;
                }
                break;
            default:
                answerWrongMethod();
                break;

        }
        clientInput.close();
        clientInput.close();
    }

    @Override
    public void run() {
        try {
            processRequest();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BadRequestException e) {
            try {
                answerBadRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
