package ru.nsu.ccfit.ermakova.ChatServer;

import java.util.TimerTask;

public class ClientsUpdater extends TimerTask {
    private ServerProcessor srvProcessor = null;

    void setServer(ServerProcessor srv) { srvProcessor = srv; }
    @Override
    public void run() {
        srvProcessor.updateClients();
    }
}
