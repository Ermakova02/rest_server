import ru.nsu.ccfit.ermakova.ChatServer.ServerProcessor;

import java.io.IOException;

public class ChatServer {
    public static void main(String[] args) throws IOException {
        ServerProcessor srvProcessor = new ServerProcessor();
        srvProcessor.launch();
    }
}
