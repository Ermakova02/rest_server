package ru.nsu.ccfit.ermakova.ChatServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.UUID;

public class ServerProcessor {
    private static final int TIMEOUT_DELAY = 5000;
    private static final int UPDATE_CLIENTS_DELAY = 3000;
    private static final int DEFAULT_PORT = 8080;

    public static final int BUFFER_WAIT_DELAY = 100;

    private List<UserData> usersList;
    private List<MessageData> messagesList;
    private int currentUserId;
    private int currentMessageId;
    private ClientsUpdater updater;
    private Timer timer;

    private int port;
    private ServerSocket srvSocket;

    public ServerProcessor() {
        usersList = new ArrayList<>();
        messagesList = new ArrayList<>();
        currentUserId = 1;
        currentMessageId = 1;
        port = DEFAULT_PORT;
        timer = new Timer(true);
        updater = new ClientsUpdater();
        updater.setServer(this);
        timer.schedule(updater, UPDATE_CLIENTS_DELAY, UPDATE_CLIENTS_DELAY);
    }

    public synchronized void updateClients() {
        long time = System.currentTimeMillis();
        for (int i = 0; i < usersList.size(); i++) {
            UserData ud = usersList.get(i);
            if (ud.online != null) {
                if ((time - ud.time > TIMEOUT_DELAY) & ud.online) {
                    ud.online = null;
                    MessageData msg = new MessageData(currentMessageId, true, ud.id, "User " + ud.username + " exited by timeout");
                    messagesList.add(msg);
                    currentMessageId++;
                }
            }
        }
    }

    public synchronized UserData userLogIn(String userName) {
        boolean userExists = false;
        UserData ud = null;
        int index = 0;
        for (; index < usersList.size(); index++)
            if (usersList.get(index).username.equals(userName)) {
                userExists = true;
                break;
            }
        if (userExists) {
            ud = usersList.get(index);
            if (ud.online == null) {
                ud.online = true;
                ud.setUpdatedTime();
                MessageData msg = new MessageData(currentMessageId, true, ud.id, "User " + ud.username + " logged in");
                messagesList.add(msg);
                currentMessageId++;
            } else if (ud.online == false) {
                ud.online = true;
                ud.setUpdatedTime();
                MessageData msg = new MessageData(currentMessageId, true, ud.id, "User " + ud.username + " logged in");
                messagesList.add(msg);
                currentMessageId++;
            } else return null;
        } else {
            ud = new UserData(currentUserId, userName);
            ud.online = true;
            usersList.add(ud);
            MessageData msg = new MessageData(currentMessageId, true, currentUserId, "User " + ud.username + " logged in");
            messagesList.add(msg);
            currentUserId++;
            currentMessageId++;
        }
        return ud;
    }

    public synchronized boolean userLogOut(UUID token) {
        for (int index = 0; index < usersList.size(); index++) {
            UserData ud = usersList.get(index);
            if (ud.token.equals(token)) {
                MessageData msg = new MessageData(currentMessageId, true, ud.id, "User " + ud.username + " logged out");
                messagesList.add(msg);
                ud.online = false;
                currentMessageId++;
                return true;
            }
        }
        return false;
    }

    public synchronized String formatUsersToJSON() {
        StringBuilder sb = new StringBuilder();
        for (int index = 0; index < usersList.size(); index++) {
            UserData ud = usersList.get(index);
            sb.append("        {\r\n");
            sb.append("            \"id\": " + Integer.toString(ud.id) + ",\r\n");
            sb.append("            \"username\": \"" + ud.username + "\",\r\n");
            sb.append("            \"online\": " + (ud.online == null ? "null" : ud.online.toString()) + "\r\n");
            if (index + 1 < usersList.size()) sb.append("        },\r\n");
            else sb.append("        }\r\n");
        }
        return sb.toString();
    }

    public synchronized void updateClientPresense(int id) {
        UserData ud = new UserData(id, "");
        if (usersList.contains(ud)) {
            usersList.get(usersList.indexOf(ud)).setUpdatedTime();
        }
    }

    public synchronized String formatMessagesToJSON(int offset, int count) {
        StringBuilder sb = new StringBuilder();
        for (int index = offset; index < messagesList.size() & index < offset + count; index++) {
            MessageData md = messagesList.get(index);
            sb.append("        {\r\n");
            sb.append("            \"id\": " + Integer.toString(md.id) + ",\r\n");
            sb.append("            \"message\": \"" + md.message + "\",\r\n");
            sb.append("            \"author\": " + Integer.toString(md.userID) + ",\r\n");
            sb.append("            \"server\": " + (md.serverMessage ? "true" : "false") + ",\r\n");
            sb.append("            \"time\": " + Long.toString(md.time) + "\r\n");
            if ((index + 1 < messagesList.size()) & (index < offset + count - 1)) sb.append("        },\r\n");
            else sb.append("        }\r\n");
        }
        return sb.toString();
    }

    public synchronized void addNewMessage(boolean server, int userId, String message) {
        MessageData md = new MessageData(currentMessageId, server, userId, message);
        messagesList.add(md);
        updateClientPresense(userId);
        currentMessageId++;
    }

    public void launch() throws IOException {
        srvSocket = new ServerSocket(port);
        while (true) {
            Socket s = srvSocket.accept();
            new SocketProcessor(s, this);
        }
    }

    public synchronized UserData findUser(UUID token) {
        for (int index = 0; index < usersList.size(); index++)
            if (usersList.get(index).token.equals(token)) {
                return usersList.get(index);
            }
        return null;
    }
}
